<?php

namespace App\Http\Controllers;

use App\Models\pembimbing;
use App\Models\perusahaan;
use App\Models\jurusan;
use App\Models\jurnal;
use App\Models\laporan;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    public function index()
    {
        $jurusan = jurusan::all()->sortBy('name');
        return view('Admin.dashboard', [
            'jurusan' => $jurusan
        ]);
    }
    public function formupdate($id)
    {
        $data = User::all()->where('id', $id)->first();
        $data1 = perusahaan::all();
        $data2 = pembimbing::all();
        $data3 = jurusan::all();

        return view('Admin.formEditSiswa', [
            'data' => $data,
            'data1' => $data1,
            'data2' => $data2,
            'data3' => $data3
        ]);
    }
    public function formupdatePembimbing($nip)
    {
        $data = User::all()->where('nis', $nip)->first();
        $data1 = perusahaan::all();
        $data2 = pembimbing::all();
        $data3 = jurusan::all();

        return view('Admin.formEditSiswa', [
            'data' => $data,
            'data1' => $data1,
            'data2' => $data2,
            'data3' => $data3
        ]);
    }
    public function update(Request $request)
    {
        $request->user()->where('id', $request->id)->update([
            'name' => $request->name,
            'nis' => $request->nis,
            'jk' => $request->jk,
            'no_telp' => $request->no_telp,
            'kelas' => $request->kelas,
            'jurusan_id' => $request->kejuruan_id,
            'status' => $request->status,
            'pembimbing_id' => $request->pembimbing_id,
            'perusahaan_id' => $request->perusahaan_id,
            'username' => $request->username,
            'email' => $request->email
        ]);
        return redirect()->back();
    }
    public function pembimbingTable()
    {
        return view('Admin.pembimbingTables');
    }
    public function insert(Request $request)
    {

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'foto_users';
        $file->move($tujuan_upload, $nama_file);
        $request->user()->insert([
            'level' => $request->level,
            'name' => $request->name,
            'jk' => $request->jk,
            'no_telp' => $request->no_telp,
            'nis' => $request->nis,
            'kelas' => $request->kelas,
            'jurusan_id' => $request->jurusan,
            'foto_users' => $nama_file,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'created_at' => now()
        ]);
        if ($request->level == 'pembimbing') {
            DB::table('pembimbings')->insert([
                'name' => $request->name,
                'jk' => $request->jk,
                'no_telp' => $request->no_telp,
                'nip' => $request->nis,
                'jurusan_id' => $request->jurusan,
                'foto' => $nama_file,
                'created_at' => now()
            ]);
        }
        return redirect('/table');
    }
    public function delete($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect('/siswaTables');
    }
    public function deletePembimbing($nip, $id)
    {
        DB::table('users')->where('pembimbing_id', $id)->update([
            'pembimbing_id' => null
        ]);
        DB::table('users')->where('nis', $nip)->delete();
        DB::table('pembimbings')->where('nip', $nip)->delete();

        return redirect('/pembimbingTables');
    }
    public function insertPerusahaan(Request $request)
    {
        $jurusan = join(',', $request->input('jurusan'));
        $kerjasama = join(',', $request->input('kerjasama'));
        DB::table('perusahaans')->insert([
            'nama_perusahaan' => $request->nama_perusahaan,
            'alamat_perusahaan' => $request->alamat_perusahaan,
            'kejuruan' => $jurusan,
            'kerjasama' => $kerjasama
        ]);
        return redirect()->back();;
    }
    public function setting()
    {
        $data = jurusan::all();
        $data1 = laporan::all()->where('id', 1);
        $data2 = jurnal::all()->where('User_id', 1);
        return view('Admin.setting', [
            'data' => $data,
            'data1' => $data1,
            'data2' => $data2
        ]);
    }
    public function insertJurusan(Request $request)
    {
        DB::table('jurusans')->insert([
            'jurusan' => $request->jurusan,
            'kepala_jurusan' => $request->kepala_jurusan
        ]);
        return redirect()->back();
    }
    public function updateJurusan(Request $request)
    {
        DB::table('jurusans')->where('id', $request->id)->update([
            'jurusan' => $request->jurusan,
            'kepala_jurusan' => $request->kepala_jurusan
        ]);
        return redirect()->back();
    }
    public function deleteJurusan($id)
    {
        DB::table('jurusans')->where('id', $id)->delete();
        return redirect()->back();
    }
    public function deletePerusahaan($id)
    {
        DB::table('perusahaans')->where('id', $id)->delete();
        return redirect()->back();
    }
    public function updatePerusahaan($id)
    {
        $data = perusahaan::all()->where('id', $id)->first();
        return view('Admin.formEditPerusahaan', ['data' => $data]);
    }
    public function editPerusahaan(Request $request)
    {
        DB::table('perusahaans')->where('id', $request->id)->update([
            'nama_perusahaan' => $request->nama_perusahaan,
            'alamat_perusahaan' => $request->alamat_perusahaan,
            'kejuruan' => $request->kejuruan,
            'kerjasama' => $request->kerjasama
        ]);
        return redirect('/table/perusahaan');
    }
}
