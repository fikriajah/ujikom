@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="container my-3">
  <div class="card">
    <div class="card-body">
      <form action="/updatePerusahaan" method="post">
          <input type="hidden" name="id" value="{{ $data->id }}">
          @csrf
          <div class="mb-3">
            <label for="" class="form-label">Nama Perusahaan</label>
            <input type="text" class="form-control" name="nama_perusahaan" value="{{ $data->nama_perusahaan }}">
          </div>
          <div class="mb-3">
            <label for="" class="form-label">Alamat Perusahaan</label>
            <input type="text" class="form-control" name="alamat_perusahaan" value="{{ $data->alamat_perusahaan }}">
          </div>
          <div class="mb-3">
            <label for="" class="form-label">Menerima Jurusan</label>
            <input type="text" class="form-control" name="kejuruan" value="{{ $data->kejuruan }}">
          </div>
          <div class="mb-3">
            <label for="" class="form-label">Kerjasama</label>
            <input type="text" class="form-control" name="kerjasama" value="{{ $data->kerjasama }}">
          </div>
          <input type="submit" value="Submit" class="btn btn-success btn-sm">
          <a href="/table/perusahaan" class="bi bi-arrow-left btn btn-danger btn-sm"> Kembali</a>
      </form>
    </div>
  </div>
</div>
@endsection