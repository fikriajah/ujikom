@extends('layouts.pembimbing.dashboard')

@section('body')

<div class="row">

  <div class="col-lg-5 d-flex justify-content-center">
    <img src="/img/4.jpg" alt="" id="image" style="width: 470px">
  </div>

  <div class="col-lg-7 mt-4">

    <div class="ml-5 mt-3">
      <span class="text-uppercase" id="title" style="font-weight: 500; margin-left: 10px">menu</span>
    </div>

    <div class="row" id="menu" style="margin-left: 33px; margin-top: 50px">
      <div class="col-xs-7" id="Jurnal">
        <a href="/dashboard/jurnalSiswa"><img src="/img/cekkegiatan.png" alt="jurnal" style="width: 65px; margin-right: 100px; margin-left:20px"></a>
        <p id="labelPilihJurusan" class="mb-3" style="margin-left: 17px; margin-top: 5px">Cek Kegiatan<br>Siswa</p>
      </div>
      <div class="col-xs-4" id="pengajuan">
        <a href="/dashboard/Pengajuan_PKL"><img src="/img/pengajuan.png" alt="jurnal" style="width: 80px; margin-right: 100px"></a>
        <p id="labelPilihJurusan" class="mb-3" style="margin-left: 17px; margin-top: -5px">Pengajuan<br>PKL</p>
      </div>
      <div class="col-xs-4" id="monitoring">
        <a href="/dashboard/monitoring"><img src="/img/monitoring.png" alt="jurnal" style="width: 70px; margin-right: 100px"></a>
        <p id="labelPilihJurusan" class="mb-3" style="margin-right: 5px">Monitoring</p>
      </div>
      <div class="col-xs-4" id="tabel">
        <a href="/table"><img src="/img/tabel.png" alt="jurnal" style="width: 80px; margin-right: 100px; margin-left: 15px"></a>
        <p id="labelPilihJurusan" class="mb-3" style="margin-top: -5px; margin-left: 38px">Tabel</p>
      </div>
      <div class="col-xs-4" id="tabel">
        <a href="/cekLaporan"><img src="/img/ceklaporan.png" alt="jurnal" style="width: 70px; margin-right: 100px"></a>
        <p id="labelPilihJurusan" class="mb-3" style="margin-top: 5px; ">Cek Laporan</p>
      </div>
      <div class="col-xs-4" id="tabel">
        <a href="/dashboard/nilai"><img src="/img/nilai2.png" alt="jurnal" style="width: 70px; margin-right: 100px"></a>
        <p id="labelPilihJurusan" class="mb-3" style="margin-top: 5px; ">Nilai Siswa</p>
      </div>
    </div>

  </div>

</div>

@endsection