@extends('layouts.pembimbing.dashboard')

@section('body')
    <div class="container">
        <table class="table  table-inverse table-responsive">
            <thead class="thead-inverse">
                <tr class="text-center">
                    <th>NIP</th>
                    <th>Nama lengkap</th>
                    <th>Kejuruan</th>
                    <th>Detail</th>
                    @if (Auth()->user()->level == 'admin')
                        <th>Edit</th>
                        <th>Delete</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                    @foreach ($data as $i)
                    <tr class="text-center">
                        <td>{{ $i->nip }}</td>
                        <td scope="row">{{ $i->name }}</td>
                        <td>{{ $i->jurusan->jurusan }}</td>
                        <td>
                            <a href="/detailPembimbing/{{ $i->id }}" class="btn btn-primary btn-sm"><i class="bi bi-info"></i></a>
                        </td>
                        @if (Auth()->user()->level == 'admin')
                            <td>
                                <a href="/editPembimbing/{{ $i->nip }}" class="btn btn-warning btn-sm"><i class="bi bi-pencil text-white"></i></a>
                            </td>
                            <td>
                                <a href="/deletePembimbing/{{ $i->nip }}/{{ $i->id }}" class="btn btn-danger btn-sm"><i class="bi bi-trash text-white"></i></a>
                            </td>
                        @endif
                        </tr>
                    @endforeach
                </tbody>
        </table>
        <a href="/table" class="btn btn-danger btn-sm">Kembali</a>
     </div>
@endsection