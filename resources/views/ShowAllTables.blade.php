@extends('layouts.siswa.dashboard')

@section('body')
{{-- <div class="container mt-2">
    <div class="row">
        <div class="col-sm-6 mt-2">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">
                        <center>Data Siswa</center>
                    </h3>
                    <table class="table table-responsive">
                        <thead class="thead-inverse text-center bg-primary text-white">
                            <tr>
                                <th>NIS</th>
                                <th>Nama Lengkap</th>
                                <th>Kelas</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach  ($siswa as $siswa)
                            <tr>
                                <td>{{ $siswa->nis }}</td>
                                <td>{{ $siswa->name }}</td>
                                <td>{{ $siswa->kelas }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="/table/siswa">Selengkapnya..</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 mt-2">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">
                        <center>Tabel Pembimbing</center>
                    </h3>
                    <table class="text-center table table-striped|sm|bordered|hover|inverse table-inverse table-responsive">
                        <thead class="thead-inverse|thead-default" style="background-color: #acacac;">
                            <tr>
                                <th>NIP</th>
                                <th>Nama Lengkap</th>
                                <th colspan="2">Kejuruan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pembimbing as $pembimbing)
                            <tr>
                                <td>{{ $pembimbing->nip }}</td>
                                <td>{{ $pembimbing->name }}</td>
                                <td>{{ $pembimbing->jurusan->jurusan }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="/table/pembimbing">Selengkapnya..</a>
                </div>
            </div>
            <div class="card mt-5">
                <div class="card-body">
                    <h3 class="card-title">
                        <center>Tabel Perusahaan</center>
                    </h3>
                    <table class="table table-striped|sm|bordered|hover|inverse table-inverse table-responsive">
                        <thead class="thead-inverse|thead-default text-center" style="background-color: #acacac">
                            <tr>
                                <th>Id</th>
                                <th>Nama Perusahaan</th>
                                <th>Alamat Perusahaan</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($perusahaan as $perusahaan)
                            <tr>
                                <td>{{ $perusahaan->id }}</td>
                                <td>{{ $perusahaan->nama_perusahaan }}</td>
                                <td>{{ $perusahaan->alamat_perusahaan }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="/table/perusahaan" >Selengkapnya.. </a>
                </div>
            </div>
        </div>

    </div>
    <a href="/home" class="btn btn-danger">Kembali</a>
</div> --}}

<div class="container mt-2">
    <div class="row">
        <div class="col-lg-4">
            <h3>All Tables</h3>

            <ul class="list-group text-center">
                <li class="list-group-item">
                    Tabel Siswa
                </li>
                <a href="/table/siswa" class="btn btn-primary btn-sm float-end"><span class="bi bi-info-circle"></span></a>
                <li class="list-group-item">
                    Tabel Pembimbing
                </li>
                <a href="/table/pembimbing" class="btn btn-primary btn-sm float-end"><span class="bi bi-info-circle"></span></a>
                
                <li class="list-group-item">
                    Tabel Perusahaan
                </li>
                <a href="/table/perusahaan" class="btn btn-primary btn-sm float-end"><span class="bi bi-info-circle"></span></a>
            </ul>
            <a href="/home" class="btn btn-danger btn-sm mt-2">Kembali</a>
        </div>
    </div>
</div>

@endsection