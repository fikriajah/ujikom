@extends('layouts.siswa.dashboard')

@section('body')
<div class="container mt-4">
    <div class="card">
        <div class="card-body">
            <hr>
            <h4>Saat ini Surat pengantar Sedang di validasi oleh pembimbing</h4>
            <P class="text-secondary">Anda bisa melanjutkan ke step selanjutnya setelah validasi selesai</P>
            <a href="/download/{{ Auth()->User()->pengantar_pkl }}" class="btn btn-warning btn-xm" style="margin-top: 10px;">Download Surat pengantar</a>

        </div>
    </div>
    <a href="/home">Kembali</a>
</div>

@endsection
